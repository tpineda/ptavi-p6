#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import sys
import socketserver


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    def handle(self):
        """handle."""
        # Escribe dirección y puerto del cliente (de tupla client_address)
        self.wfile.write(b"Hemos recibido tu peticion")

        # Leyendo línea a línea lo que nos envía el cliente
        linea = self.rfile.read()
        method = linea.decode('utf-8').split(' ')[0]
        PUERTO = sys.argv[2].split(':')[0]

        print("El cliente nos manda " + linea.decode('utf-8'))

        if method == 'INVITE':
            self.wfile.write(b"\r\nContent-Lenght:  \r\n")
            self.wfile.write(b"SIP/2.0 100 TRYING\r\n")
            self.wfile.write(b"SIP/2.0 180 RING\r\n")
            self.wfile.write(b"SIP/2.0 200 OK\r\n")
            self.wfile.write(b"v = 0\r\n")
            self.wfile.write(b"o = 0\r\n")
            self.wfile.write(b"s = misesion\r\n")
            self.wfile.write(b"t = 0\r\n")
            self.wfile.write(b"m = audio " + PUERTO + b" RTP\r\n")

        elif method == 'BYE':
            self.wfile.write(b"SIP/2.0 200 OK\r\n")

        elif method == 'ACK':
            self.wfile.write()


if __name__ == "__main__":

    # Creamos servidor de eco y escuchamos
    serv = socketserver.UDPServer(('', 6001), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    serv.serve_forever()
