#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""
import sys
import socket

# Cliente UDP simple.

# Dirección IP del servidor.
if __name__ == "__main__":
    "Intentamos reconocemos los datos que hemos introducido en el terminal"
    try:
        METHOD = sys.argv[1]
        RECEIVER = sys.argv[2].split('@')
        IP_RECEIVER = RECEIVER[1].split(':')[0]
        PORT = sys.argv[2].split(':')[0]

    except SyntaxError:
        sys.exit('Usage: python3 client.py method receiver@IP:SIPport')

# Contenido que vamos a enviar
if METHOD == "INVITE":
    LINEA = METHOD + ' sip:' + RECEIVER + '@' + 'IP_RECEIVER' + ' SIP/2.0'

if METHOD == "BYE":
    LINEA = METHOD + ' sip:' + RECEIVER + '@' + 'IP_RECEIVER' + ' SIP/2.0'

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((IP_RECEIVER, PORT))

    print("Enviando: " + LINEA)
    my_socket.send(bytes(LINEA, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)

    print('Recibido -- ', data.decode('utf-8'))
    if METHOD == 'INVITE':
        TRYING = "SIP/2.0 100 TRYING"
        RING = "SIP/2.0 180 RING"
        OK = "SIP/2.0 200 OK"

        print("Content-Lenght: ")
        print("v = 0/r/n")
        print("o = " + str(RECEIVER) + IP_RECEIVER + "/r/n")
        print("s = misesion/r/n")
        print("t = 0/r/n")
        print("m = audio " + str(PORT) + " RTP/r/n")
    print("Terminando socket...")

print("Fin.")
